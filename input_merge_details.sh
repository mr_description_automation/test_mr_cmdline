#!/bin/bash

#get project id from user
read -p "Enter the Gitlab Project ID : " PROJECT_ID 

#get source branch from user
read -p "Enter the source branch : " SOURCE_BRANCH

#keeping target branch by default as master,can change if needed
read -p "Enter the target branch : " TARGET_BRANCH

#read title for mr
read -p "Enter the Merge Request Title : " MR_TITLE

#display all the templates available
#import from template_source.sh
source template_source.sh

#show the merge details prior to creating the merge request
echo "======================================================"
echo "MERGE REQUEST DETAILS "
echo "======================================================"
echo "Project ID : $PROJECT_ID"
echo "Source Branch : $SOURCE_BRANCH"
echo "Target Branch : $TARGET_BRANCH"
echo "Merge Request Title : $MR_TITLE"
echo "Description Template : $SELECTED_TEMPLATE"

export PROJECT_ID
export SOURCE_BRANCH 
export TARGET_BRANCH
export MR_TITLE
export SELECTED_TEMPLATE
