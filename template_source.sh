#!/bin/bash

#setting the template paths
TEMPLATE_SOURCE=".gitlab/merge_request_templates"
TEMPLATES=($(ls -1 "$TEMPLATE_SOURCE"/*.md))

# echo "${TEMPLATES[@]}"
# printf "'%s'\n" "${TEMPLATES[@]}"

echo "Choose a Description Template"

for index in "${!TEMPLATES[@]}"; 
do
    value=$((index + 1))
    echo "$value : ${TEMPLATES[index]}"
done

while true;do
    read -p "Enter your choice : " selected_template_index

    #check if the index is valid
    #else print invalid template choice
    if (($selected_template_index >= 1 && $selected_template_index<=${#TEMPLATES[@]})); then
        SELECTED_TEMPLATE="${TEMPLATES[selected_template_index - 1]}"
        export SELECTED_TEMPLATE
        echo "Selected template: $SELECTED_TEMPLATE"
        break
    else
        echo "Invalid template choice. Please select a valid index."
    fi
done
