**Summary**

A brief summary of the feature being implemented and the changes introduced by this merge request.

**Related Issue(s)**

- [ ] Specify the related issue(s) or feature request(s) associated with this feature implementation. 
- [ ] Include the issue number, title, and a brief summary.

**Description**

- Provide a detailed description of the feature being implemented and the changes made in this merge request. 
- Explain the purpose and goals of the feature, any requirements or specifications, and the approach taken to implement it.

**Design Decisions**

Explain any design decisions or considerations made during the implementation of the feature. This could include 

- architectural choices
- technical trade-offs
- user experience considerations

**Implementation Details**

Include information on any new functionality, modified components, or database schema changes.

**Testing**

Include information on any specific test cases executed, platforms tested, and any relevant test results.

**Documentation Updates**

Specify any documentation updates or additions made to reflect the new feature. 
This could include 
 
- API documentation
- user guides
- internal developer documentation.

**Reviewer(s)**
_Mention the names or usernames of the assigned reviewer(s) for this merge request._

**Screenshots**
If applicable, provide any relevant screenshots, images, or attachments that help visualize or explain the new feature.
