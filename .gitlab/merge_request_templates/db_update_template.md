**Summary**

Brief summary of the database schema update and changes introduced.

**Related Issue(s)**

Specify the related issue(s) or task(s) prompting the schema update.

**Description**

- [ ] Detailed description of the database schema update and its purpose.
- [ ] Explain the specific changes made to the schema and their significance.

**Design Decisions**

- [ ] Explain the design choices made during the schema update process.
- [ ] Include considerations such as data integrity, performance, or scalability.

**Schema Changes**

- [ ] Specify the specific changes made to the database schema.
- [ ] Describe new tables, modified columns, or any other schema modifications.

**Data Migration**


- [ ] Explain the process for migrating existing data to accommodate the schema update.
- [ ] Describe any data transformation or migration scripts utilized.

**Documentation Updates**

Specify updates made to any documentation affected by the schema update.
This could include data dictionaries, entity relationship diagrams, or migration guides.

**Reviewer(s)**

Mention the assigned reviewer(s) for this merge request.
