**Summary**

A brief summary of the bug being fixed and the changes introduced by this merge request.

**Related Issue(s)**

Specify the related issue(s) or bug report(s) associated with this bug fix. Include the issue number, title, and a brief summary.

**Description**


- [ ] Provide a detailed description of 
    - the bug being addressed 
    - the changes made in this merge request. 
- [ ] Include information about the impact of the bug, steps to reproduce it, and any relevant context.

**Root Cause Analysis**

Explain the root cause of the bug, if known. Describe any investigation or analysis performed to identify the underlying issue.

**Solution**

- [ ] Describe the approach taken to fix the bug. 
- [ ] Explain the changes made to the codebase, any new logic implemented, and how the fix resolves the issue.

**Testing**

- [ ] Detail the testing approach taken to validate the bug fix. 
- [ ] Include information on any specific test cases executed, platforms tested, and any relevant test results.

**Reviewer(s)**

_Mention the names or usernames of the assigned reviewer(s) for this merge request._

**Screenshots/Attachments**

If applicable, provide any relevant screenshots, logs, or attachments that demonstrate the bug and its resolution.

