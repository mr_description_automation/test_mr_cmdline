#!/bin/bash

#read the merge request details
source input_merge_details.sh
#echo $SELECTED_TEMPLATE_PATH 

#set the constants for the file
GITLAB_API_URL="https://gitlab.com/api/v4"
ACCESS_TOKEN="glpat-5QQHBNppoxXknAGKkBfg"
TEMPLATE_FILE=$(cat "$SELECTED_TEMPLATE")
#echo "$TEMPLATE_FILE"

#gitlab api call to create merge request
curl --request POST --header "PRIVATE-TOKEN: $ACCESS_TOKEN" \
	--data "source_branch=$SOURCE_BRANCH" \
	--data "target_branch=$TARGET_BRANCH" \
	--data-urlencode "title=$MR_TITLE" \
	--data-urlencode "description=$TEMPLATE_FILE" \
	"$GITLAB_API_URL/projects/$PROJECT_ID/merge_requests"
